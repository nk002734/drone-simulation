package droneSimulation;

public class ConsoleCanvas {

	private char[][] arr;

	public ConsoleCanvas(int xSize, int ySize) {
		arr = new char[xSize + 2][ySize + 2];
		for (int j = 0; j <= ySize + 1; j++) { // for every row
			for (int i = 0; i <= xSize + 1; i++) { // loop each column
				if (i == 0 || i == xSize + 1 || j == 0 || j == ySize + 1) {
					arr[i][j] = '#';
				} else {
					arr[i][j] = ' ';
				}
			}
		}
	}

	public void showIt(int x, int y, char D) {
		this.arr[x + 1][y + 1] = D;
	}

	public String toString() {
		String output = "";
		for (int j = 0; j < arr[0].length; j++) {
			for (int i = 0; i < arr.length; i++) {
				output += arr[i][j];
			}
			output += '\n';
		}
		return output;
	}

	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas(80, 8); // create a canvas
		c.showIt(40, 3, 'D'); // add a Drone at 4,3
		c.showIt(20, 3, 'D'); // add a Drone at 4,3
		c.showIt(47, 3, 'D'); // add a Drone at 4,3

		System.out.println(c.toString()); // display result
	}
}
