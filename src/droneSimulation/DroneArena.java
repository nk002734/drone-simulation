package droneSimulation;

import java.util.Random;
import java.util.ArrayList; // import the ArrayList class
import java.util.List;

public class DroneArena {
	private int xSize, ySize;
	private ArrayList<Drone> drones;

	public DroneArena(int ixSize, int iySize) {
		xSize = ixSize;
		ySize = iySize;
		drones = new ArrayList<Drone>(); // Create an ArrayList object
		
	}

	public int getxSize() {
		return xSize;
	}

	public int getySize() {
		return ySize;
	}
	public ArrayList<Drone> getDrones() {
		return drones;
	}

	public void addDrone() {
		Drone newDrone;
		Random randomGenerator = new Random();
		int xVal = randomGenerator.nextInt(xSize);
		int yVal = randomGenerator.nextInt(ySize);
		Direction dir = Direction.values()[randomGenerator.nextInt(Direction.values().length)];

		boolean foundFreeSpace = false;
		do {
			if (this.getDroneAt(xVal, yVal) == null) {
				newDrone = new Drone(xVal, yVal, dir);
				drones.add(newDrone);
				foundFreeSpace = true;
			} else {
				xVal = randomGenerator.nextInt(xSize);
				yVal = randomGenerator.nextInt(ySize);
			}
		} while (foundFreeSpace == false);
	}
	
	public void loadDrone(List<String> droneData) {
		Drone newDrone = new Drone(Integer.parseInt(droneData.get(0)), Integer.parseInt(droneData.get(1)), Direction.valueOf(droneData.get(2)));
		drones.add(newDrone);
	}
	public boolean canMoveHere(int x, int y) {
		if (x == -1 || y == -1 || x == xSize || y == ySize || this.getDroneAt(x, y) != null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * show all the drones in the interface
	 * 
	 * @param c the canvas in which drones are shown
	 */
	public void showDrones(ConsoleCanvas c) {
		// << loop through all the Drones calling the displayDrone method >>
		for (Drone aDrone : drones) {
			aDrone.displayDrone(c);
		}

	}

	/**
	 * search arraylist of drones to see if there is a drone at x,y
	 * 
	 * @param x
	 * @param y
	 * @return null if no Drone there, otherwise return drone
	 */
	public Drone getDroneAt(int x, int y) {
		// << return either Drone at x,y or null >>
		Drone output = null;
		for (Drone aDrone : drones) {
			if (aDrone.isHere(x, y))
				return aDrone;
		}
		return output;
	}
	
	public String toFile() {
		String output = xSize + "," + ySize + "\n";
		for (Drone aDrone : drones) {
			output += aDrone.toFile();
		}
		return output;

	}

	public String toString() {
		String output = "Arena is " + xSize + " wide and " + ySize + " tall. ";
		for (Drone aDrone : drones) {
			output = output + "\n" + aDrone.toString();
		}
		return output + "\n";
	}

	public void moveAllDrones() {
		for (Drone aDrone : drones) {
			aDrone.tryToMove(this);
		}
	}

	public static void main(String[] args) {
		DroneArena a = new DroneArena(3, 3); // create drone arena
		a.addDrone();
		a.addDrone();
		a.addDrone();
		a.addDrone();
		System.out.println(a.toString()); // print where is
	}

}