package droneSimulation;

import java.util.Random;

public enum Direction {
	North, East, South, West {
		@Override
		public Direction next() {
			return values()[0];
		}
	};

	public Direction next() {
		return values()[ordinal() + 1];
	}

	public Direction getRandomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
}
