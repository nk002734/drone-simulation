package droneSimulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import javax.swing.JFileChooser;

/**
 * Simple program to show arena with multiple drones
 * 
 * @author shsmchlr
 *
 */
public class DroneInterface {

	private Scanner s; // scanner used for input from user
	private DroneArena myArena; // arena in which drones are shown
	/**
	 * constructor for DroneInterface sets up scanner used for input and the arena
	 * then has main loop allowing user to enter commands
	 */
	public DroneInterface() {
		Scanner s = new Scanner(System.in); // set up scanner for user input
		int xSize = 0, ySize = 0;
		// String input;
		boolean valid = false;
		while (valid == false) {
			try {
				valid = true;
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				System.out.print("Enter x size: ");
				String input = br.readLine();
				xSize = Integer.parseInt(input);
			} catch (NumberFormatException | IOException ex) {
				valid = false;
				// ex.printStackTrace();
				System.out.println("That is not an integer. Try again");
			}
		}
		valid = false;
		while (valid == false) {
			try {
				valid = true;
				BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
				System.out.print("Enter y size: ");
				String input2 = br2.readLine();
				ySize = Integer.parseInt(input2);

			} catch (NumberFormatException | IOException ex) {
				valid = false;
				// ex.printStackTrace();
				System.out.println("That is not an integer. Try again");
			}
		}
		myArena = new DroneArena(xSize, ySize);
		
		
		char ch = ' ';
		do {
			System.out.print(
					"Enter (A)dd drone, get (I)nformation, (D)isplay drones, (M)ove drones, (N)ew arena, (S)ave arena, (L)oad arena or e(X)it > ");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
			case 'A':
			case 'a':
				myArena.addDrone(); // add a new drone to arena
				break;
			case 'I':
			case 'i':
				System.out.print(myArena.toString());
				break;
			case 'X':
			case 'x':
				ch = 'X'; // when X detected program ends
				break;
			case 'D':
			case 'd':
				this.doDisplay();
				break;
			case 'M':
			case 'm':
				for (int i = 0; i < 10; i++) {
					myArena.moveAllDrones();
					this.doDisplay();
					System.out.print(myArena.toString());
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} // do nothing for 1000 milliseconds (1 second)
				}
				break;
			case 'N':
			case 'n':
				/*DroneArena newArena;
				int xSize = 0, ySize = 0;
				// String input;
				 */
				
				valid = false;
				while (valid == false) {
					try {
						valid = true;
						BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
						System.out.print("Enter x size: ");
						String input = br.readLine();
						xSize = Integer.parseInt(input);
					} catch (NumberFormatException | IOException ex) {
						valid = false;
						// ex.printStackTrace();
						System.out.println("That is not an integer. Try again");
					}
				}
				valid = false;
				while (valid == false) {
					try {
						valid = true;
						BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
						System.out.print("Enter y size: ");
						String input2 = br2.readLine();
						ySize = Integer.parseInt(input2);

					} catch (NumberFormatException | IOException ex) {
						valid = false;
						// ex.printStackTrace();
						System.out.println("That is not an integer. Try again");
					}
				}
				myArena = new DroneArena(xSize, ySize);
				break;
			case 'S':
			case 's':
				saveFile();
				break;
			case 'L':
			case 'l':
				loadFile();
				break;

			}
		} while (ch != 'X'); // test if end

		s.close(); // close scanner
	}

	/**
	 * Display the drone arena on the console
	 * 
	 */

	public void loadFile() {
		JFileChooser chooser = new JFileChooser();
		int returnVal;
		List<List<String>> records = new ArrayList<>();

		returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selFile = chooser.getSelectedFile();
			System.out.println("You chose to open this file: " + selFile.getName());
			if (selFile.isFile()) { // exists and is a file
				try (BufferedReader br = new BufferedReader(new FileReader(selFile.getAbsoluteFile()))) {
					String line;
					while ((line = br.readLine()) != null) {
						String[] values = line.split(",");
						records.add(Arrays.asList(values));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		myArena = new DroneArena(Integer.parseInt(records.get(0).get(0)), Integer.parseInt(records.get(0).get(1)));
		for (int i = 1; i < records.size(); i++)
			myArena.loadDrone(records.get(i));
	}

	public void saveFile() {
		JFileChooser chooser = new JFileChooser();

		String data = myArena.toFile();
		int returnVal = chooser.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selFile = chooser.getSelectedFile();
			File currDir = chooser.getCurrentDirectory();
			String filename = currDir.getAbsolutePath() + '/' + selFile.getName() + ".drn";

			try {
				File myObj = new File(filename);
				if (myObj.createNewFile()) {
					System.out.println("File created: " + myObj.getName());
				} else {
					System.out.println("File already exists.");
				}
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}

			try {
				FileWriter myWriter = new FileWriter(filename);
				myWriter.write(data);
				myWriter.close();
				System.out.println("Successfully wrote to the file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}

			System.out.println(
					"You chose to save into file: " + selFile.getName() + " in the dir " + currDir.getAbsolutePath());
		}

	}

	void doDisplay() {
		// determine the arena size
		// hence create a suitable sized ConsoleCanvas object
		ConsoleCanvas c = new ConsoleCanvas(myArena.getxSize(), myArena.getySize());
		// call showDrones suitably
		myArena.showDrones(c);
		// then use the ConsoleCanvas.toString method
		System.out.print(c.toString());
	}

	public static void main(String[] args) {
		DroneInterface r = new DroneInterface(); // just call the interface
	}

}
