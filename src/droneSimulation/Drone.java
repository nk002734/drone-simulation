package droneSimulation;

public class Drone {
	private int x, y, droneID, dx, dy;
	private static int droneCount = 0;
	private Direction dir;

	public Drone(int bx, int by, Direction bdir) {
		x = bx;
		y = by;
		dir = bdir;
		droneID = droneCount++;
		dx = 0;
		dy = 0;
	}
	
	public void resetDroneID() {
		droneID = 0;
	}
	public void tryToMove(DroneArena dArena) {
		boolean canMove = false;
		while (canMove == false) {
			switch (dir) {
			case North:
				dy++;			
				break;
			case East:
				dx++;
				break;
			case South:
				dy--;
				break;
			case West:
				dx--;
				break;
			}
			if (dArena.canMoveHere(x + dx, y + dy)) {
				x += dx;
				y += dy;
				canMove = true;
			}
			else {
				dir = dir.next();
			}
		}
		dx = 0;
		dy = 0;
	}
	
	/**
	 * Is the drone at this x,y position
	 * 
	 * @param sx x position
	 * @param sy y position
	 * @return true if drone is at sx,sy, false otherwise
	 */
	public boolean isHere(int sx, int sy) {
		if (x == sx & y == sy)
			return true;
		else
			return false;
	}

	public String toString() {

		return "Drone " + droneID + " is at " + x + ", " + y + " going " + dir.toString() + ".";
	}

	public String toFile() {
		return x + "," + y + "," + dir.toString() + "\n";
	}
	/**
	 * display the drone in the canvas
	 * 
	 * @param c the canvas used
	 */
	public void displayDrone(ConsoleCanvas c) {
		// << call the showIt method in c to put a D where the drone is
		c.showIt(this.x, this.y, 'D');
	}

	public static void main(String[] args) {
		Drone a = new Drone(5, 3, Direction.South); // create drone
		System.out.println(a.toString()); // print where is
	}
}
